# --------------------------------------------------
#                     HEYOKA ')
# --------------------------------------------------
# Clase Heyoka
#     Permite la ejecución del método Heyoka, mediante
#     el uso de un constructor y la configuración de
#     ciertas variables, por ejemplo:
#
# heyoka = LibHeyoka.Heyoka('y, 9.8*sin(x)', a, b, h , VI);
# heyoka.SetVars(t=None, x=1, y=1, z=None)
# m, n, thy = heyoka.ejecutar()
#
# Donde las variables m, n y thy contendrán la siguiente información:
#    m  : valores para la cual fue evaluada la ecuación diferencial.
#    n  : el resultado de la evaluación
#    thy: el tiempo que duró la ejecución del proceso.
#
import heyoka as hy
from sympy import *
import numpy as np
import LibTimeProcess as tmp


class Heyoka:
    __f = None
    __vi = None

    __ta = None

    __a = None
    __b = None
    __h = None

    '''
    f  : representa la función a evaluar (es un string)
    vi : representa los valores incialaes (es un arreglo) 
    '''
    def __init__(self, f, a, b, h, vi):
        self.__f = self.__getFuncionHeyoka(f)
        self.__vi = vi
        self.__a = a
        self.__b = b
        self.__h = h

    '''
    getFuncionHeyoka
    ---------------------------------------------------
    Transforma una ecuación en texto a una expresión
    que puede ser evaluada por Heyoka
    params:
        f: función en formato texto, ejemplo:
               '3*x+2*cos(y)'
           En caso de querer expresar un arreglo de funciones
           estas deberán estar separadas por comas
                '3*x+2*cos(y),4*exp(x)-2'
    '''
    def __getFuncionHeyoka(self, f):
        fa = f.split(",")
        fh = []
        for i in range(len(fa)):
            fh.append(hy.from_sympy(sympify(fa[i])))

        return fh

    '''
    SetVars
    ---------------------------------------------------
    Permite configurar las variables simbólicas que serán usadas
    en la ecuación o sistema de ecuaciones
    '''
    def SetVars(self, t=None, x=None, y=None, z=None):
        if t is not None and x is None and y is None and z is None:
            t = hy.make_vars("t")

            self.__ta = hy.taylor_adaptive(
                sys=[(t),
                     self.__f],
                state=self.__vi,
            )

        if t is not None and x is not None and y is None and z is None:
            t, x = hy.make_vars("t", "x")

            self.__ta = hy.taylor_adaptive(
                sys=[(t, x),
                     self.__f],
                state=self.__vi,
            )

        if t is not None and x is not None and y is not None and z is None:
            t, x, y = hy.make_vars("t", "x", "y")

            self.__ta = hy.taylor_adaptive(
                sys=[(t, x, y),
                     self.__f],
                state=self.__vi,
            )

        if t is not None and x is not None and y is not None and z is not None:
            t, x, y, z = hy.make_vars("t", "x", "y", "z")

            self.__ta = hy.taylor_adaptive(
                sys=[(t, x, y, z),
                     self.__f],
                state=self.__vi,
            )

        if t is not None and x is None and y is not None and z is None:
            t, y = hy.make_vars("t", "y")

            self.__ta = hy.taylor_adaptive(
                sys=[(t, y),
                     self.__f],
                state=self.__vi,
            )

        if t is not None and x is None and y is None and z is not None:
            t, z = hy.make_vars("t", "z")

            self.__ta = hy.taylor_adaptive(
                sys=[(t, z),
                     self.__f],
                state=self.__vi,
            )

        if t is not None and x is None and y is not None and z is not None:
            t, y, z = hy.make_vars("t", "y", "z")

            self.__ta = hy.taylor_adaptive(
                sys=[(t, y, z),
                     self.__f],
                state=self.__vi,
            )

        if t is not None and x is not None and y is None and z is not None:
            t, x, z = hy.make_vars("t", "x", "z")

            self.__ta = hy.taylor_adaptive(
                sys=[(t, x, z),
                     self.__f],
                state=self.__vi,
            )

        if t is None and x is not None and y is None and z is None:
            x = hy.make_vars("x")

            self.__ta = hy.taylor_adaptive(
                sys=[(x),
                     self.__f],
                state=self.__vi,
            )

        if t is None and x is not None and y is not None and z is None:
            x, y = hy.make_vars("x", "y")

            self.__ta = hy.taylor_adaptive(
                sys=[(x, y),
                     self.__f],
                state=self.__vi,
            )

        if t is None and x is not None and y is not None and z is not None:
            x, y, z = hy.make_vars("x", "y", "z")

            self.__ta = hy.taylor_adaptive(
                sys=[(x, y, z),
                     self.__f],
                state=self.__vi,
            )

        if t is None and x is not None and y is None and z is not None:
            x, z = hy.make_vars("x", "z")

            self.__ta = hy.taylor_adaptive(
                sys=[(x, z),
                     self.__f],
                state=self.__vi,
            )

        if t is None and x is None and y is not None and z is None:
            y = hy.make_vars("y")

            self.__ta = hy.taylor_adaptive(
                sys=[(y),
                     self.__f],
                state=self.__vi,
            )

        if t is None and x is None and y is not None and z is not None:
            y, z = hy.make_vars("y", "z")

            self.__ta = hy.taylor_adaptive(
                sys=[(y, z),
                     self.__f],
                state=self.__vi,
            )

        if t is None and x is None and y is None and z is not None:
            z = hy.make_vars("z")

            self.__ta = hy.taylor_adaptive(
                sys=[(z),
                     self.__f],
                state=self.__vi,
            )

    '''
    ejecutar
    -------------------------------------------------------
    Función que ejecuta o inicia el método de Taylos de la 
    librería Heyoka.
    
    Retorna:
        grid  : los valores para los que fue evaluado
        out   : el resultado obtenido de la ejecución del método
        tiempo: el tiempo de duración del proceso.
    '''
    def ejecutar(self):
        inicio = tmp.getTime()
        self.__ta.step(write_tc=True)
        grid = np.arange(self.__a, self.__b + self.__h, self.__h)
        out = self.__ta.propagate_grid(grid)

        fin = tmp.getTime()
        tiempo = tmp.getProcessTime(inicio, fin)

        return grid, out[4], tiempo

# printTablaEulerHeyoka(m, n, out[4])
