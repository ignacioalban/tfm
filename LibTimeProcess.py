import time as tm

'''
getTime
--------------------------------------------
Get hora de inicio o fin del proceso
'''
def getTime():
    return tm.time()


'''
getProcessTime
---------------------------------------------
getTiempoTranscurrido desde el inicio al fin
params:
    inicio : Tiempo de inicico del proceso
    fin    : Tiempo de finalización del proceso
return:
    Tiempo entre el inicio y el fin
'''
def getProcessTime(inicio, fin):
    return fin - inicio

