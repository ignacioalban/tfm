# LibMethods
# -----------------------------------------------------------------
# Librería que contiene los diferentes métodos numéricos para
# resolución de Problemas de Valor Inicial (PVI) con los métodos
# de un paso y multipaso.
#
from sympy import *
import LibTimeProcess as tmp
from LibArrays import *
import numpy as np
import math


t = Symbol('t')
x = Symbol('x')
y = Symbol('y')
z = Symbol('z')

def convierteTextoFuncion(f):
    fa = f.split(",")
    fh = []
    for i in range(len(fa)):
        fh.append(sympify(fa[i]))

    return fh

def convierteTextoVI(vi):
    return eval(vi)

'''
f_CreaEvals:
    Permite crear los pntos de evaluación para la función f(t, x, y, z, ...)
'''
def f_CreaEvals(size, X, Y):
    l = {}
    if size == 1:
        l = {t: X, x: Y[0]}
    elif size == 2:
        l = {t: X, x: Y[0], y: Y[1]}
    elif size == 3:
        l = {t: X, x: Y[0], y: Y[1], z: Y[2]}

    return l

def f_evalFuncionAndDerivada(f, df, X, Y):
    n = len(Y)
    ffun = np.zeros(n)
    dfun = np.zeros((len(df), len(df[0])))
    dim = np.shape(Y)

    for m in range(n):
        if len(dim) > 1:
            ffun[m] = f[m].subs(f_CreaEvals(n, X, Y[0]))
        else:
            ffun[m] = f[m].subs(f_CreaEvals(n, X, Y))
        n1 = len(df[m])
        for r in range(n1):
            if len(dim) > 1:
                dfun[m][r] = df[m][r].subs(f_CreaEvals(n, X, Y[0]))
            else:
                dfun[m][r] = df[m][r].subs(f_CreaEvals(n, X, Y))
    return ffun, dfun                                          
   
   

  
'''


Euler:
    Método de resolución de PVI de un paso.
Params:
    f = Función o Funciones simbólica a evaluar
    a = Punto inicial del intervalo a evaluar
    b = Punto final del intervalo a evaluar
    N = Cantidad de divisiones a realizar
    Y0 = Valor o valores iniciales.
'''
def Euler(f, a, b, N, Y0):
    inicio = tmp.getTime()
    h = (b - a) / N
    X = np.arange(a, b + h, h)
    n = 1

    isArray = isinstance(Y0, list)
    if isArray is True:
        Y = np.zeros((N + 1, len(Y0)))
        n = len(Y0)

        for k in range(n):
            Y[0][k] = float(Y0[k])

    for k in range(N):
        for m in range(n):
            Y[k + 1][m] = Y[k][m] + h * f[m].subs(f_CreaEvals(n, X[k], Y[k]))

    fin = tmp.getTime()
    tiempo = tmp.getProcessTime(inicio, fin)
    return X, Y, tiempo

'''
Heun:
    Método de resolución de PVI de un paso.
Params:
    f = Función o Funciones simbólica a evaluar
    a = Punto inicial del intervalo a evaluar
    b = Punto final del intervalo a evaluar
    N = Cantidad de divisiones a realizar
    Y0 = Valor o valores iniciales.
'''
def Heun(f, a, b, N, Y0):
    inicio = tmp.getTime()
    h = (b - a) / N
    X = np.arange(a, b + h, h)
    n = 1

    Y = np.zeros((N + 1, len(Y0)))
    K1 = np.zeros((N + 1, len(Y0)))
    K2 = np.zeros((N + 1, len(Y0)))
    n = len(Y0)

    for k in range(n):
        Y[0][k] = float(Y0[k])

    for k in range(N):
        for m in range(n):
            K1[k][m] = h * f[m].subs(f_CreaEvals(n, X[k], Y[k]))

        for m in range(n):
            K2[k][m] = h * f[m].subs(f_CreaEvals(n, X[k + 1], Y[k] + K1[k]))

        for m in range(n):
            Y[k + 1][m] = Y[k][m] +(K1[k][m] + K2[k][m])/2

    fin = tmp.getTime()
    tiempo = tmp.getProcessTime(inicio, fin)
    return X, Y, tiempo


'''
Ralston:
    Método de resolución de PVI de un paso.
Params:
    f = Función o Funciones simbólica a evaluar
    a = Punto inicial del intervalo a evaluar
    b = Punto final del intervalo a evaluar
    N = Cantidad de divisiones a realizar
    Y0 = Valor o valores iniciales.
'''
def Ralston(f, a, b, N, Y0):
    inicio = tmp.getTime()
    h = (b - a) / N
    X = np.arange(a, b + h, h)
    n = 1

    Y = np.zeros((N + 1, len(Y0)))
    K1 = np.zeros((N + 1, len(Y0)))
    K2 = np.zeros((N + 1, len(Y0)))
    n = len(Y0)

    for k in range(n):
        Y[0][k] = float(Y0[k])

    for k in range(N):
        for m in range(n):
            K1[k][m] = f[m].subs(f_CreaEvals(n, X[k], Y[k]))

        for m in range(n):
            K2[k][m] = f[m].subs(f_CreaEvals(n, (3/4) * h  + X[k], Y[k] + (3/4) * K1[k] * h))

        for m in range(n):
            Y[k + 1][m] = Y[k][m] +(((1/3) * K1[k][m] + (2/3) * K2[k][m]) * h)
           

    fin = tmp.getTime()
    tiempo = tmp.getProcessTime(inicio, fin)
    return X, Y, tiempo
    
   
       
'''
Rungekutta:
    Método de resolución de PVI de un paso.
Params:
    f = Función o Funciones simbólica a evaluar
    a = Punto inicial del intervalo a evaluar
    b = Punto final del intervalo a evaluar
    N = Cantidad de divisiones a realizar
    Y0 = Valor o valores iniciales.
'''

    
def RungeKutta(f, a, b, N, Y0):
    inicio = tmp.getTime()
    h = (b - a) / N
    X = np.arange(a, b + h, h)
    n = 1

    Y = np.zeros((N + 1, len(Y0)))
    K1 = np.zeros((N + 1, len(Y0)))
    K2 = np.zeros((N + 1, len(Y0)))
    K3 = np.zeros((N + 1, len(Y0)))
    K4 = np.zeros((N + 1, len(Y0)))
    n = len(Y0)

    for k in range(n):
        Y[0][k] = float(Y0[k])

    for k in range(N):
        for m in range(n):
            K1[k][m] = f[m].subs(f_CreaEvals(n, X[k], Y[k]))

        for m in range(n):
            K2[k][m] = f[m].subs(f_CreaEvals(n, h/2  + X[k], Y[k] +h/2 * K1[k]))

        for m in range(n):
            K3[k][m] = f[m].subs(f_CreaEvals(n, h/2  + X[k],  Y[k] + h/2 * K2[k]))
        
        for m in range(n):
            K4[k][m] = f[m].subs(f_CreaEvals(n, X[k+1], Y[k] +   K3[k] * h))
        
        for m in range(n):
            Y[k + 1][m] = Y[k][m] + ((K1[k][m] + K2[k][m] * 2 + K3[k][m] * 2 + K4[k][m]) * (h/6))
           

    fin = tmp.getTime()
    tiempo = tmp.getProcessTime(inicio, fin)
    return X, Y, tiempo


'''
RungekuttaButcher (RK5):
    Método de resolución de PVI de un paso.
Params:
    f = Función o Funciones simbólica a evaluar
    a = Punto inicial del intervalo a evaluar
    b = Punto final del intervalo a evaluar
    N = Cantidad de divisiones a realizar
    Y0 = Valor o valores iniciales.
'''


def RungeKuttaButcher(f, a, b, N, Y0):
    inicio = tmp.getTime()
    h = (b - a) / N
    X = np.arange(a, b + h, h)
    n = 1

    Y = np.zeros((N + 1, len(Y0)))
    K1 = np.zeros((N + 1, len(Y0)))
    K2 = np.zeros((N + 1, len(Y0)))
    K3 = np.zeros((N + 1, len(Y0)))
    K4 = np.zeros((N + 1, len(Y0)))
    K5 = np.zeros((N + 1, len(Y0)))
    K6 = np.zeros((N + 1, len(Y0)))
    n = len(Y0)

    for k in range(n):
        Y[0][k] = float(Y0[k])

    for k in range(N):
        for m in range(n):
            K1[k][m] = f[m].subs(f_CreaEvals(n, X[k], Y[k]))

        for m in range(n):
            K2[k][m] = f[m].subs(f_CreaEvals(n, X[k] + h/4, Y[k] + h * K1[k]/4))

        for m in range(n):
            K3[k][m] = f[m].subs(f_CreaEvals(n, X[k] + h/4, Y[k] + h * (K1[k]/8 + K2[k]/8)))

        for m in range(n):
            K4[k][m] = f[m].subs(f_CreaEvals(n, X[k] + h/2, Y[k] + h * (-K2[k]/2 + K3[k])))

        for m in range(n):
            K5[k][m] = f[m].subs(f_CreaEvals(n, X[k] + 3*h/4, Y[k] + 3 * h * (K1[k] + 3*K4[k])/16))

        for m in range(n):
            K6[k][m] = f[m].subs(f_CreaEvals(n, X[k] + h, Y[k] + h * (-3*K1[k] + 2*K2[k] + 12*K3[k] - 12*K4[k] + 8*K5[k])/7))

        for m in range(n):
            Y[k + 1][m] = Y[k][m] + h*(7*K1[k][m] + 32*K3[k][m] + 12*K4[k][m] + 32*K5[k][m] + 7*K6[k][m])/90

    fin = tmp.getTime()
    tiempo = tmp.getProcessTime(inicio, fin)
    return X, Y, tiempo

    
'''
AdamsBashfort:
    Método de resolución de PVI multipaso.
Params:
    f = Función o Funciones simbólica a evaluar
    a = Punto inicial del intervalo a evaluar
    b = Punto final del intervalo a evaluar
    N = Cantidad de divisiones a realizar
    Y0 = Valor o valores iniciales.
    linea 222 n=3 para generar los primeros pasos del rungekutta
'''

    
def AdamsBashfort(f, a, b, N, Y0):
    inicio = tmp.getTime()
    h = (b - a) / N
    X = np.arange(a, b + h, h)
    n = 1
    
    Y = np.zeros((N + 1, len(Y0)))
    K1 = np.zeros((N + 1, len(Y0)))
    K2 = np.zeros((N + 1, len(Y0)))
    K3 = np.zeros((N + 1, len(Y0)))
    K4 = np.zeros((N + 1, len(Y0)))
    ff = np.zeros((N + 1, len(Y0)))
    n = len(Y0)
 
 
    for k in range(n):
        Y[0][k] = float(Y0[k])

    for k in range(3):
        for m in range(n):
            K1[k][m] = f[m].subs(f_CreaEvals(n, X[k], Y[k]))

        for m in range(n):
            K2[k][m] = f[m].subs(f_CreaEvals(n, h/2  + X[k], Y[k] +h/2 * K1[k]))

        for m in range(n):
            K3[k][m] = f[m].subs(f_CreaEvals(n, h/2  + X[k],  Y[k] + h/2 * K2[k]))
        
        for m in range(n):
            K4[k][m] = f[m].subs(f_CreaEvals(n, X[k+1], Y[k] +   K3[k] * h))
        
        for m in range(n):
            Y[k + 1][m] = Y[k][m] + ((K1[k][m] + K2[k][m] * 2 + K3[k][m] * 2 + K4[k][m]) * (h/6))
       
    for m in range(n):
        ff[0][m]=f[m].subs(f_CreaEvals(n, X[0], Y[0]))
        ff[1][m]=f[m].subs(f_CreaEvals(n, X[1], Y[1]))
        ff[2][m]=f[m].subs(f_CreaEvals(n, X[2], Y[2]))
    
    for k in range(3 , N , 1):
        for m in range(n):
            ff[k][m]=f[m].subs(f_CreaEvals(n, X[k], Y[k]))
       
        for m in range(n):
            Y[k + 1][m] = Y[k][m] + (h/24)*(55* ff[k][m]-59*ff[k-1][m]+37*ff[k-2][m]-9*ff[k-3][m])
     
      
    fin = tmp.getTime()
    tiempo = tmp.getProcessTime(inicio, fin)
    return X, Y, tiempo
 
    
'''
AdamsMoulton:
    Método de resolución de PVI multipaso.
Params:
    f = Función o Funciones simbólica a evaluar
    a = Punto inicial del intervalo a evaluar
    b = Punto final del intervalo a evaluar
    N = Cantidad de divisiones a realizar
    Y0 = Valor o valores iniciales.
    linea 222 n=3 para generar los primeros pasos del rungekutta
'''


def AdamsMoulton(f, df, a, b, N, Y0):
    inicio = tmp.getTime()
    h = (b - a) / N
    X = np.arange(a, b + h, h)

    Y = np.zeros((N + 1, len(Y0)))
    K1 = np.zeros((N + 1, len(Y0)))
    K2 = np.zeros((N + 1, len(Y0)))
    K3 = np.zeros((N + 1, len(Y0)))
    K4 = np.zeros((N + 1, len(Y0)))

    n = len(Y0)

    for k in range(n):
        Y[0][k] = float(Y0[k])

    for k in range(2):
        for m in range(n):
            K1[k][m] = f[m].subs(f_CreaEvals(n, X[k], Y[k]))

        for m in range(n):
            K2[k][m] = f[m].subs(f_CreaEvals(n, h / 2 + X[k], Y[k] + h / 2 * K1[k]))

        for m in range(n):
            K3[k][m] = f[m].subs(f_CreaEvals(n, h / 2 + X[k], Y[k] + h / 2 * K2[k]))

        for m in range(n):
            K4[k][m] = f[m].subs(f_CreaEvals(n, X[k + 1], Y[k] + K3[k] * h))

        for m in range(n):
            Y[k + 1][m] = Y[k][m] + ((K1[k][m] + K2[k][m] * 2 + K3[k][m] * 2 + K4[k][m]) * (h / 6))

    n1 = len(df[0])
    fk = np.zeros(n)
    fkm1 = np.zeros(n)
    fkm2 = np.zeros(n)
    t = np.zeros((n, n1))
    fx = np.zeros(n)
    dfx = np.zeros((n, n1))
    d = np.zeros((n, n1))
    y1 = np.zeros(n)

    for k in range(2, N):
        ex = 1
        iter = 0
        tol = 1e-14
        maxiter = 100

        for m in range(n):
            y1[m] = Y[k][m]

        for m in range(n):
            fk[m] = f[m].subs(f_CreaEvals(n, X[k], Y[k]))

        for m in range(n):
            fkm1[m] = f[m].subs(f_CreaEvals(n, X[k - 1], Y[k - 1]))

        for m in range(n):
            fkm2[m] = f[m].subs(f_CreaEvals(n, X[k - 2], Y[k - 2]))

        ffun, dfun = f_evalFuncionAndDerivada(f, df, X[k + 1], Y[k])
        fk = fk.transpose()
        fkm1 = fkm1.transpose()
        fkm2 = fkm2.transpose()
        ffun = ffun.transpose()

        for m in range(n):
            dim = np.shape(y1)
            if len(dim) > 1:
                fx[m] = y1[0][m] - Y[k][m] - h * (fkm2[m] - 5 * fkm1[m] + 19 * fk[m] + 9 * ffun[m]) / 24

            else:
                fx[m] = y1[m] - Y[k][m] - h * (fkm2[m] - 5 * fkm1[m] + 19 * fk[m] + 9 * ffun[m]) / 24

        efx = np.linalg.norm(fx)

        while iter < maxiter and efx > tol and ex > tol:
            for m in range(n):
                for r in range(n1):
                    dfx[m][r] = (1 - h * 9 * dfun[m][r] / 24)
                    d[m][r] = fx[m] / dfx[m][r]

            t = np.subtract(y1, d.transpose())
            # t=t.transpose()

            ffun, dfun = f_evalFuncionAndDerivada(f, df, X[k + 1], t)
            # print(t,ffun,dfun)

            for m in range(n):
                dim = np.shape(y1)
                if len(dim) > 1:
                    fx[m] = t[0][m] - Y[k][m] - h * (fkm2[m] - 5 * fkm1[m] + 19 * fk[m] + 9 * ffun[m]) / 24
                else:
                    fx[m] = t[0][m] - Y[k][m] - h * (fkm2[m] - 5 * fkm1[m] + 19 * fk[m] + 9 * ffun[m]) / 24

            efx = np.linalg.norm(fx)
            ex = np.linalg.norm(np.subtract(t, y1))

            iter = iter + 1
            for m in range(n):
                y1[m] = t[0][m]


        for m in range(n):
            Y[k + 1][m] = y1[m]

    fin = tmp.getTime()
    tiempo = tmp.getProcessTime(inicio, fin)
    return X, Y, tiempo


'''
Adams Bashfort Moulton:
    Método de resolución de PVI multipaso.
Params:
    f = Función o Funciones simbólica a evaluar
    a = Punto inicial del intervalo a evaluar
    b = Punto final del intervalo a evaluar
    N = Cantidad de divisiones a realizar
    Y0 = Valor o valores iniciales.
    linea 222 n=3 para generar los primeros pasos del rungekutta
'''
def AdamsBashfortMoulton(f, df, a, b, N, Y0):
    inicio = tmp.getTime()
    h = (b - a) / N
    X = np.arange(a, b + h, h)

    Y = np.zeros((N + 1, len(Y0)))
    K1 = np.zeros((N + 1, len(Y0)))
    K2 = np.zeros((N + 1, len(Y0)))
    K3 = np.zeros((N + 1, len(Y0)))
    K4 = np.zeros((N + 1, len(Y0)))
    p = np.zeros((N + 1, len(Y0)))
    p1 = np.zeros((N + 1, len(Y0)))
    p2 = np.zeros((N + 1, len(Y0)))
    p3 = np.zeros((N + 1, len(Y0)))
    p4 = np.zeros((N + 1, len(Y0)))
    n = len(Y0)

    for k in range(n):
        p[0][k] = float(Y0[k])

    for k in range(3):
        for m in range(n):
            K1[k][m] = f[m].subs(f_CreaEvals(n, X[k], p[k]))

        for m in range(n):
            K2[k][m] = f[m].subs(f_CreaEvals(n, h / 2 + X[k], p[k] + h / 2 * K1[k]))

        for m in range(n):
            K3[k][m] = f[m].subs(f_CreaEvals(n, h / 2 + X[k], p[k] + h / 2 * K2[k]))

        for m in range(n):
            K4[k][m] = f[m].subs(f_CreaEvals(n, X[k + 1], p[k] + K3[k] * h))

        for m in range(n):
            p[k + 1][m] = p[k][m] + ((K1[k][m] + K2[k][m] * 2 + K3[k][m] * 2 + K4[k][m]) * (h / 6))

    
    for k in range(3,N):
        for m in range(n):
            K1[k][m] = 55 * f[m].subs(f_CreaEvals(n, X[k], p[k]))

        for m in range(n):
            K2[k][m] = -59 * f[m].subs(f_CreaEvals(n, X[k - 1], p[k - 1]))

        for m in range(n):
            K3[k][m] = 37 * f[m].subs(f_CreaEvals(n, X[k - 2], p[k - 2] ))

        for m in range(n):
            K4[k][m] = - 9 * f[m].subs(f_CreaEvals(n, X[k - 3], p[k - 3]))

        for m in range(n):
            p[k + 1][m] = p[k][m] + ((K1[k][m] + K2[k][m]  + K3[k][m] + K4[k][m]) * (h / 24))
            

    for k in range(N):
        for m in range(n):       
             Y[k][m] = p[k][m]


    for k in range(3,N):
        for m in range(n):
            p1[0][m] = 9 * f[m].subs(f_CreaEvals(n, X[k + 1], p[k + 1]))

        for m in range(n):
            p2[0][m] = 19 * f[m].subs(f_CreaEvals(n, X[k], Y[k]))

        for m in range(n):
            p3[0][m] = 5 * f[m].subs(f_CreaEvals(n, X[k - 1], Y[k - 1] ))

        for m in range(n):
            p4[0][m] = f[m].subs(f_CreaEvals(n, X[k - 2], Y[k - 2]))

        for m in range(n):
            Y[k + 1][m] = Y[k][m] + ((p1[0][m] + p2[0][m]  - p3[0][m] + p4[0][m]) * (h / 24)) 
            

       
    fin = tmp.getTime()
    tiempo = tmp.getProcessTime(inicio, fin)
    return X, Y, tiempo
  

    
def printTabla(x, y):
    print('-----------------------------------------')
    print('     X         |           y             ')
    print('-----------------------------------------')
    for i in range(len(x)):
        X = round(x[i],2)
        print(f'{X:14} | {y[i][0]:24}')

    print('-----------------------------------------')


def printTablaEulerHeyoka(x, y, h):
    print('--------------------------------------------------------------------')
    print('     X         |       Y(Euler)          |         Y(Heyoka)        ')
    print('--------------------------------------------------------------------')
    for i in range(len(x)):
        print(f'{x[i]:14} | {y[i][0]:24} | {h[i][0]:24}')

    print('--------------------------------------------------------------------')