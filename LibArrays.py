#
#
#
#
import numpy as np
import math


'''
f_Append:
    Permite agregar un grupo de datos a un arreglo, dicho dato puede ser
    un número cualquiera o un arreglo
'''
def f_Append(value, size):
    x = []
    for k in range(size):
        x.append(value)

    return x

'''
f_Filtra:
    Permite obtener los valores de un arreglo por sus índices pares
    incluyendo el índice 0.
'''
def f_Filtra(Y):
    l = len(Y)
    n = math.ceil(l/2)
    out = np.zeros(n)
    k = 0
    for i in range(l):
        if i % 2 == 0:
            out[k] = Y[i]
            k = k + 1
    return out