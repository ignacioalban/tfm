import json
import bottle as bt
from LibMethods import *
from LibCalculoErrorOrden import *


# the decorator
def enable_cors(fn):
    def _enable_cors(*args, **kwargs):
        # set CORS headers
        bt.response.headers['Access-Control-Allow-Origin'] = '*'
        bt.response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
        bt.response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

        if bt.request.method != 'OPTIONS':
            # actual request; reply with the actual response
            return fn(*args, **kwargs)

    return _enable_cors


@bt.route('/comparacionMetodos', method=["POST", "OPTIONS"])
@enable_cors
def comparaMetodosPVI():
    bt.response.headers['Content-Type'] = 'application/json'

    respuesta = json.load(bt.request.body);

    funcion = respuesta['funcion']
    vi = respuesta['vi']
    a = respuesta['ii']
    b = respuesta['if']
    N = respuesta['N']

    euler = respuesta['eu']
    heun = respuesta['he']
    ralston = respuesta['rl']
    rungekutta = respuesta['rk']
    rkbutcher = respuesta['rk5']
    adambashford = respuesta['ab']
    adammoulton = respuesta['am']
    abmoulton = respuesta['abm']

    f = convierteTextoFuncion(funcion)
    VI = convierteTextoVI(vi)

    a = int(a)
    b = int(b)
    N = int(N)

    mE = None; mH = None; mR = None; mRK = None; mAB = None; mAM = None; mABM = None; mRK5 = None
    nE = None; nH = None; nR = None; nRK = None; nAB = None; nAM = None; nABM = None; nRK5 = None
    tE = None; tH = None; tR = None; tRK = None; tAB = None; tAM = None; tABM = None; tRK5 = None
    errEu = None; errHe = None; errRa = None; errRK = None; errAB = None; errAM = None; errABM = None; errRK5 = None
    ordEu = None; ordHe = None; ordRa = None; ordRK = None; ordAB = None; ordAM = None; ordABM = None; ordRK5 = None

    cnt = 0;
    x = None
    Ne = 20
    if euler is True:
        mE, nE, tE = Euler(f, a, b, N, VI)
        
        # Calculando el error de euler
        m1, n1, t = Euler(f, a, b, 2 * Ne, VI)
        m2, n2, t = Euler(f, a, b, 4 * Ne, VI)
        m3, n3, t = Euler(f, a, b, 8 * Ne, VI)
        m4, n4, t = Euler(f, a, b, 16 * Ne, VI)
        m5, n5, t = Euler(f, a, b, 32 * Ne, VI)
        
        errEu, ordEu = calculaErrorOrden(nE[:, 0], n1[:, 0], n2[:, 0], n3[:, 0], n4[:, 0], n5[:, 0])
        
        mE = mE.tolist()
        nE = nE.tolist()
        cnt = len(mE)
        x = mE

    if heun is True:
        mH, nH, tH = Heun(f, a, b, N, VI)
        
        # Calculando el error de Heun
        m1, n1, t = Heun(f, a, b, 2 * Ne, VI)
        m2, n2, t = Heun(f, a, b, 4 * Ne, VI)
        m3, n3, t = Heun(f, a, b, 8 * Ne, VI)
        m4, n4, t = Heun(f, a, b, 16 * Ne, VI)
        m5, n5, t = Heun(f, a, b, 32 * Ne, VI)

        errHe, ordHe = calculaErrorOrden(nH[:, 0], n1[:, 0], n2[:, 0], n3[:, 0], n4[:, 0], n5[:, 0])
        
        mH = mH.tolist()
        nH = nH.tolist()
        cnt = len(mH)
        x = mE

    if ralston is True:
        mR, nR, tR = Ralston(f, a, b, N, VI)
        
        # Calculando el error de Ralston
        m1, n1, t = Ralston(f, a, b, 2 * Ne, VI)
        m2, n2, t = Ralston(f, a, b, 4 * Ne, VI)
        m3, n3, t = Ralston(f, a, b, 8 * Ne, VI)
        m4, n4, t = Ralston(f, a, b, 16 * Ne, VI)
        m5, n5, t = Ralston(f, a, b, 32 * Ne, VI)

        errRa, ordRa = calculaErrorOrden(nR[:, 0], n1[:, 0], n2[:, 0], n3[:, 0], n4[:, 0], n5[:, 0])
        
        mR = mR.tolist()
        nR = nR.tolist()
        cnt = len(mR)
        x = mR

    if rungekutta is True:
        mRK, nRK, tRK = RungeKutta(f, a, b, N, VI)
        
        # Calculando el error de RungeKutta
        m1, n1, t = RungeKutta(f, a, b, 2 * Ne, VI)
        m2, n2, t = RungeKutta(f, a, b, 4 * Ne, VI)
        m3, n3, t = RungeKutta(f, a, b, 8 * Ne, VI)
        m4, n4, t = RungeKutta(f, a, b, 16 * Ne, VI)
        m5, n5, t = RungeKutta(f, a, b, 32 * Ne, VI)

        errRK, ordRK = calculaErrorOrden(nRK[:, 0], n1[:, 0], n2[:, 0], n3[:, 0], n4[:, 0], n5[:, 0])
        
        mRK = mRK.tolist()
        nRK = nRK.tolist()
        cnt = len(mRK)
        x = mRK

    if rkbutcher is True:
        mRK5, nRK5, tRK5 = RungeKutta(f, a, b, N, VI)

        # Calculando el error de RungeKutta
        m1, n1, t = RungeKuttaButcher(f, a, b, 2 * Ne, VI)
        m2, n2, t = RungeKuttaButcher(f, a, b, 4 * Ne, VI)
        m3, n3, t = RungeKuttaButcher(f, a, b, 8 * Ne, VI)
        m4, n4, t = RungeKuttaButcher(f, a, b, 16 * Ne, VI)
        m5, n5, t = RungeKuttaButcher(f, a, b, 32 * Ne, VI)

        errRK5, ordRK5 = calculaErrorOrden(nRK5[:, 0], n1[:, 0], n2[:, 0], n3[:, 0], n4[:, 0], n5[:, 0])

        mRK5 = mRK.tolist()
        nRK5 = nRK.tolist()

        cnt = len(mRK5)
        x = mRK5

    if adambashford is True:
        mAB, nAB, tAB = AdamsBashfort(f, a, b, N, VI)
        
        # Calculando el error de AdamBashford
        m1, n1, t = AdamsBashfort(f, a, b, 2 * Ne, VI)
        m2, n2, t = AdamsBashfort(f, a, b, 4 * Ne, VI)
        m3, n3, t = AdamsBashfort(f, a, b, 8 * Ne, VI)
        m4, n4, t = AdamsBashfort(f, a, b, 16 * Ne, VI)
        m5, n5, t = AdamsBashfort(f, a, b, 32 * Ne, VI)

        errAB, ordAB = calculaErrorOrden(nAB[:, 0], n1[:, 0], n2[:, 0], n3[:, 0], n4[:, 0], n5[:, 0])
        
        mAB = mAB.tolist()
        nAB = nAB.tolist()
        cnt = len(mAB)
        x = mAB

    if adammoulton is True:
        mAM, nAM, tAM = AdamsMoulton(f, a, b, N, VI)

        # Calculando el error de AdamBashford
        m1, n1, t = AdamsMoulton(f, a, b, 2 * Ne, VI)
        m2, n2, t = AdamsMoulton(f, a, b, 4 * Ne, VI)
        m3, n3, t = AdamsMoulton(f, a, b, 8 * Ne, VI)
        m4, n4, t = AdamsMoulton(f, a, b, 16 * Ne, VI)
        m5, n5, t = AdamsMoulton(f, a, b, 32 * Ne, VI)

        errAM, ordAM = calculaErrorOrden(nAM[:, 0], n1[:, 0], n2[:, 0], n3[:, 0], n4[:, 0], n5[:, 0])

        mAM = mAM.tolist()
        nAM = nAM.tolist()
        cnt = len(mAM)
        x = mAM

    if abmoulton is True:
        mABM, nABM, tABM = AdamsBashfortMoulton(f, a, b, N, VI)

        # Calculando el error de AdamBashford
        m1, n1, t = AdamsBashfortMoulton(f, a, b, 2 * Ne, VI)
        m2, n2, t = AdamsBashfortMoulton(f, a, b, 4 * Ne, VI)
        m3, n3, t = AdamsBashfortMoulton(f, a, b, 8 * Ne, VI)
        m4, n4, t = AdamsBashfortMoulton(f, a, b, 16 * Ne, VI)
        m5, n5, t = AdamsBashfortMoulton(f, a, b, 32 * Ne, VI)

        errABM, ordABM = calculaErrorOrden(nABM[:, 0], n1[:, 0], n2[:, 0], n3[:, 0], n4[:, 0], n5[:, 0])

        mABM = mABM.tolist()
        nABM = nABM.tolist()
        cnt = len(mABM)
        x = mABM

    data = {"cantidad": cnt,
            "x": x,
            "Euler": {"x": mE, "resultado": nE, "tiempo": tE, "error": errEu, "orden": ordEu},
            "Heun": {"x": mH, "resultado": nH, "tiempo": tH, "error": errHe, "orden": ordHe},
            "Ralston": {"x": mR, "resultado": nR, "tiempo": tR, "error": errRa, "orden": ordRa},
            "RungeKutta": {"x": mRK, "resultado": nRK, "tiempo": tRK, "error": errRK, "orden": ordRK},
            "RKButcher": {"x": mRK5, "resultado": nRK5, "tiempo": tRK5, "error": errRK5, "orden": ordRK5},
            "AdamBashford": {"x": mAB, "resultado": nAB, "tiempo": tAB, "error": errAB, "orden": ordAB},
            "AdamMoulton": {"x": mAM, "resultado": nAM, "tiempo": tAM, "error": errAM, "orden": ordAM},
            "AdamBashfordMoulton": {"x": mABM, "resultado": nABM, "tiempo": tABM, "error": errABM, "orden": ordABM}
            }

    return json.dumps(data)



app = bt.default_app()
if __name__ == "__main__":
    bt.run(host='localhost', port=8085)
