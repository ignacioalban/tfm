# Archivo usado para realizar los test de los diferenes métodos numéricos
# La ecuación diferencial a testear es la siguiente:
#    y(x)' = y(x) - x^2 + 1
# Para definir la función debe tener en cuenta lo siguiente:
#    Los exponenciales como x^4, se representan como x**4
#    Expresiones como e^x, se representan como exp(x)
# La variable independiente siempre la definiremos con la letra t,
# mientras que la variables dependientes con las x, y o z
# por ejemplo la expresión:
#    x(t)' = 3x(t) + 2y(t) - (2t^2 + 1)e^(2t)
#    deberá ser escrita como:
#    f = 3*x + 2*y - (2*t**2 + 1)*exp(2*t)
# Si se pretende resolver un sistema de ecuaciones diferenciales, se deberá
# definir la función en términos matriciales, por ejemplo:
#    f = [3*x + 2*y - (2*t**2 + 1)*exp(2*t),
#         4*x + y + (t**2 + 2*t - 4)*exp(2*t)]
# El valor Inicial generalmente es un número, pero cuando trabajamos con
# un sistema de ecuaciones, éste deberá ser también un arreglo, con tantos
# valores como variables dependientes que tenga nuestro sistema, en el
# ejemplo anterior, la expresión para los valores iniciales sería por ejemplo:
#     [1, 0]
# Se incluyen los métodos:
#  - Euler
#
from LibMethods import *
#import heyoka as hy
import LibHeyoka

a = 0
b = 2
N = 20
h = 0.1

# Definición de los valores iniciales
VI = [math.pi/6, 0]
#VI = [1, 3]
#VI = eval('[math.pi/6, 0]')

# Definición de la función f a evaluar
f = [y, 9.8*sin(x)]
df = [[0*x, 1+0*x], [9.8*cos(x), 0*x]]                                       
#f  = [y, -x-2*sin(t)]
df = [[0*x,1+0*x],[-1+0*x,0*x]]                               
#y2;-y1-2*sin(x)  --> este es el modelo en matlab, no descomentar.
#f = sympify('[y, 9.8*sin(x)]')

# Utilización del método de Euler
m, n, t = Euler(f, a, b, N, VI)
te = t

printTabla(m, n)

m, n, t = Heun(f, a, b, N, VI)
th = t

printTabla(m, n)

m, n, t = Ralston(f, a, b, N, VI)
th = t

printTabla(m, n)

m, n, t = RungeKutta(f, a, b, N, VI)
th = t

printTabla(m, n)

m, n, t = AdamsBashfort(f, a, b, N, VI)
th = t

printTabla(m, n)

m, n, t = AdamsMoulton(f, df, a, b, N, VI)
th = t

printTabla(m, n)

m, n, t = AdamsBashfortMoulton(f, df, a, b, N, VI)
th = t

printTabla(m, n)

#heyoka = LibHeyoka.Heyoka('y, 9.8*sin(x)', a, b, h , VI);
#heyoka.SetVars(t=None, x=1, y=1, z=None)
#m, n, thy = heyoka.ejecutar()
#printTabla(m, n)

#print(str(te) + " | " + str(th) + " | " + str(thy))

