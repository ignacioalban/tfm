from LibMethods import *
from LibArrays import *


def calculaErrorOrden(Y1, Y2, Y3, Y4, Y5, Y6):
    error = np.zeros(6)
    ouErr = ['', '', '', '', '', '']
    orden = np.zeros(6)
    ouOrd = ['', '', '', '', '', '']

    for i in range(6):
        if i == 0:
            error[i] = None
        if i == 1:
            YE = f_Filtra(Y2)
            error[i] = abs(np.subtract(YE, Y1)).max()
        if i == 2:
            YE = f_Filtra(Y3)
            error[i] = abs(np.subtract(YE, Y2)).max()
        if i == 3:
            YE = f_Filtra(Y4)
            error[i] = abs(np.subtract(YE, Y3)).max()
        if i == 4:
            YE = f_Filtra(Y5)
            error[i] = abs(np.subtract(YE, Y4)).max()
        if i == 5:
            YE = f_Filtra(Y6)
            error[i] = abs(np.subtract(YE, Y5)).max()


    for i in range(4):
        orden[i+2] = math.log(error[i+1]/error[i + 2], 2)

    for i in range(6):
        if i == 0:
            ouErr[i] = ''
            ouOrd[i] = ''
        else:
            ouErr[i] = str(error[i])
            if i == 1:
                ouOrd[i] = ''
            else:
                ouOrd[i] = str(orden[i])

    return ouErr, ouOrd