import xlsxwriter as excel
from LibProgressBar import *


class Excel:
    __workbook = None
    __worksheet = None

    def __init__(self):
        self.__workbook = excel.Workbook('tfm-tiempos.xlsx')
        self.__worksheet = self.__workbook.add_worksheet()

        bold = self.__workbook.add_format({'bold': True})

        self.__worksheet.write('A1', 'N', bold)
        self.__worksheet.write('B1', 'Euler', bold)
        self.__worksheet.write('C1', 'Heun', bold)
        self.__worksheet.write('D1', 'Ralston', bold)
        self.__worksheet.write('E1', 'RungeKutta', bold)
        self.__worksheet.write('F1', 'RK Burchlet', bold)
        self.__worksheet.write('G1', 'AdamsBashford', bold)
        self.__worksheet.write('H1', 'AdamsMoulton', bold)
        self.__worksheet.write('I1', 'ABMoulton', bold)
        self.__worksheet.write('J1', 'Heyoka', bold)


    def generar(self, data):
        finalizado = False

        i = 2
        l = len(data)
        printProgressBar(0, l, prefix='Progreso:', suffix='Completo', length=50)
        for dt in data:
            self.__worksheet.write('A' + str(i), i-1)
            self.__worksheet.write('B' + str(i), dt['Euler'])
            self.__worksheet.write('C' + str(i), dt['Heun'])
            self.__worksheet.write('D' + str(i), dt['Ralston'])
            self.__worksheet.write('E' + str(i), dt['RK4'])
            self.__worksheet.write('F' + str(i), dt['RK5'])
            self.__worksheet.write('G' + str(i), dt['AB'])
            self.__worksheet.write('H' + str(i), dt['AM'])
            self.__worksheet.write('I' + str(i), dt['ABM'])
            self.__worksheet.write('J' + str(i), dt['Heyoka'])
            i = i + 1
            printProgressBar(i, l, prefix='Progreso:', suffix='Completo', length=50)

        self.__workbook.close()
        print('\r\n')
        return finalizado

