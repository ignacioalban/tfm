from LibMethods import *
from LibCalculoErrorOrden import *
import LibHeyoka
import LibExcel
from LibProgressBar import *


nd = 1000

a = 0
b = 2
N = 20
h = 0.1

VI = [math.pi/6, 0]
f = [y, 9.8*sin(x)]
df = [[0*x, 1+0*x], [9.8*cos(x), 0*x]]

heyoka = LibHeyoka.Heyoka('y, 9.8*sin(x)', a, b, h, VI);
heyoka.SetVars(t=None, x=1, y=1, z=None)

excel = LibExcel.Excel()

data = []

print('Iniciando la generación de datos.')
printProgressBar(0, nd, prefix='Progreso:', suffix='Completo', decimals=2, length=50)
for i in range(nd):
    m, n, tEul = Euler(f, a, b, N, VI)
    m, n, tHeu = Heun(f, a, b, N, VI)
    m, n, tRal = Ralston(f, a, b, N, VI)
    m, n, tRK4 = RungeKutta(f, a, b, N, VI)
    m, n, tRK5 = RungeKutta(f, a, b, N, VI)
    m, n, tAB4 = AdamsBashfort(f, a, b, N, VI)
    m, n, tAM4 = AdamsMoulton(f, df, a, b, N, VI)
    m, n, tABM = AdamsBashfordMoulton(f, df, a, b, N, VI)

    m, n, tHYK = heyoka.ejecutar()

    data.append({"Euler": tEul,
                 "Heun": tHeu,
                 "Ralston": tRal,
                 "RK4": tRal,
                 "RK5": tRK5,
                 "AB": tAB4,
                 "AM": tAM4,
                 "ABM": tABM,
                 "Heyoka": tHYK})
    #print('\r Obteniendo registro ' + str(i + 1) + ' de ' + str(nd), end='\r')
    printProgressBar(i + 1, nd, prefix='Progreso:', suffix='Completo', decimals=2, length=50)

print('\r\n')
print('Generación de datos finalizada.')

print('Iniciando creación de archivo de Excel')
excel.generar(data)
print('Archivo de excel generado')